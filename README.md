![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/dreyri/matter.svg)

*This repository is no longer maintained.* This project was valuable for learning more about ECS architecture. Rather than using a loop, I'm switching to an event based approach with dynamically scheduled systems.

![Matter Logo](./logo/matter.png =250x)
